<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\InvoiceController;

Route::post('invoice', [InvoiceController::class, 'store'])->name('invoice.store');

