### Coding Challenge Document

You can see complete document below:
***
**Sections**
1. InvoiceController

In this class, you can see a construct method which has service injection for calculating invoice. First of all, in store method, input data will passed and then a calss named InvoiceRequest will validate that. After validation, InvoiceService will call and calculate the invoice of charging process. Finally, in output, you will see an object which will contain an overall price & rate values.  
2. InvoiceService

In this class, after receive input, the timestamp values will convert from ISO 8601 to UTC and then the difference between them will calculate. Finally, the overall value of invoice will return.
3. InvoiceRequest

In this class, all fields of the input of InvoiceController will validate. If occurred any validation error, it will be showed proper message, and you can see it in output.  
4. Unit Tests

In this directory, there are two classes: 

* InvoiceServiceTest

In this class, there are three tests for checking InvoiceService class.
* InvoiceControllerTest

In this class, there are four tests for checking InvoiceController class.

5. Route

In this directory, there is a class named api.php for determining the api route with address '/invoice' with POST method.

***
**Some Tips**
There is a gitlab address for checking related repository as below:

Gitlab repository address (https://gitlab.com/dedicated-projects/coding-challenge.git)

**Note:** as there isn't other tasks, so the gitlab repository has not another branch and the only branch is master.

For running all tests, just enter the following command in cmd:

**vendor\bin\phpunit**
