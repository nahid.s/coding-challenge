<?php

namespace App\Http\Controllers;

use Illuminate\Http\Response;
use App\Services\InvoiceService;
use App\Http\Requests\InvoiceRequest;

class InvoiceController extends Controller
{
    public InvoiceService $invoiceService;

    public function __construct(InvoiceService $invoiceService)
    {
        $this->invoiceService = $invoiceService;
    }

    public function store(InvoiceRequest $request)
    {
        try{
            $validated = $request->validated();

            if (isset($validated->message)) {
                return response()->json($validated, Response::HTTP_UNPROCESSABLE_ENTITY);
            }

            $overall = $this->invoiceService->calculateInvoice($request->cdr, $request->rate);

            $calculatedInvoice = [
                "overall" => round($overall, 2),
                "components" => $request->rate
            ];

            return response()->json($calculatedInvoice, Response::HTTP_CREATED);
        }
        catch(\Exception $e) {
            return $e->getMessage();
        }

    }
}
