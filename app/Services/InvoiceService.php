<?php

namespace App\Services;

use Carbon\Carbon;

class InvoiceService
{
    public function calculateInvoice($cdr, $rate)
    {
        //Convert ISO 8601 to UTC timezone (string to dateTime) & calculate the difference between them
        $timestampStart = Carbon::parse($cdr['timestampStart']);
        $timestampStop = Carbon::parse($cdr['timestampStop']);
        $timestampValue = $timestampStop->diffInMinutes($timestampStart);

        //Calculate the difference between start & start meter
        $meterValue = ($cdr['meterStop'] - $cdr['meterStart']) / 1000;

        //Get the rate values
        $energy = $rate['energy'];
        $valuePerMinute = ($rate['time']) / 60;
        $transaction = $rate['transaction'];

        //Calculate & return the overall value
        return ($energy*$meterValue) + ($valuePerMinute*$timestampValue) + ($transaction);
    }
}
