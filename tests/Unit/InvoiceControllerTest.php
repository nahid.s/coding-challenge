<?php

namespace Tests\Unit;

use App\Http\Requests\InvoiceRequest;
use Illuminate\Support\Facades\Validator;
use Tests\TestCase;

class InvoiceControllerTest extends TestCase
{
    /**
     * @test
     */
    public function invalid_route_method()
    {
        $this->get(route('invoice.store'))
            ->assertStatus(405);
    }

    /**
     * @test
     */
    public function successful_redirection_to_invoice_route()
    {
        $response = $this->call('POST', route('invoice.store'));
        $this->assertEquals(302, $response->getStatusCode());
    }

    /**
     * @test
     */
    public function successful_invoice_validation()
    {
        $input = [
            'rate' => ["energy" => 0.3, "time" => 2, "transaction" => 1 ],
            'cdr' => [
                "meterStart"=> 1204307,
                "timestampStart"=> "2021-04-05T10:04:00Z",
                "meterStop"=> 1215230,
                "timestampStop" => "2021-04-05T11:27:00Z"
            ],
        ];

        $this->post(route('invoice.store'), $input)
            ->assertStatus(201);
    }

    /**
     * @test
     */
    public function validation_error_occurred()
    {
        $validator = Validator::make(
            [
                'rate' => ["energy" => 0.3, "time" => 2, "transaction" => 1 ],
            ],
            (new InvoiceRequest())->rules());

        $this->assertFalse($validator->passes());
    }
}
